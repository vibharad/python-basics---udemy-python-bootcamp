import re

def parse_bytes(streambytes):
	byte_pattern = re.compile(r"\b[0-1]{8}\b")
	match = byte_pattern.findall(streambytes)

	if match:
		return match
	else:
		return []

print(parse_bytes("11010110 213 324142"))
print(parse_bytes("my data is: 11001010 1101010011001 10101001"))
print(parse_bytes("asdas"))
