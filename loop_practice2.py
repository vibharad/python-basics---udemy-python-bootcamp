#right angle triangle of emojis/special symbols usign for & while 
print("\nPRINTING USING FOR LOOP\n")
for symb in range(1,11):
	print("@" * symb)

print("\nPRINTING USING WHILE LOOP\n")
symb = 1
while symb<11:
	print(" " * (11 - symb) + "@" * symb)
	symb+=1

# print ("\nPRINTING PYRAMID\n")
# symb = 1
# while symb<11:
# 	gaps = (11 - symb)//2
# 	if symb%2==0:
# 		print(" "+" " * gaps + "^" * symb + " " * gaps)
# 	elif symb%2==1:
# 		print(" " * gaps + "^" * symb + " " * gaps)
# 	symb += 1

print("\n COMBINING FOR AND WHILE\n")
left = 1;
right = 1;
while left<11 and right>0:
	print(" " * (11 - left) + "@" * left + "@" * right)
	left+=1
	right+=1

