def find_factors(num):
	factor = 1
	factors = []
	while (factor <= num):
		if num%factor == 0:
			factors.append(factor)
		factor+=1
	return factors

print(find_factors(23))
print(find_factors(34))
print(find_factors(36))