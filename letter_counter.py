def letter_counter(mystr):
	letter_counter.letters = {}
	for letter in mystr:
		if letter.lower() in letter_counter.letters.keys():
			letter_counter.letters[letter.lower()] += 1
		else:
			letter_counter.letters[letter.lower()] = 1
	def counted(mychar):
		if mychar.lower() in letter_counter.letters.keys():
			print(letter_counter.letters[mychar.lower()])
			return letter_counter.letters[mychar.lower()]
		else:
			return None
	return counted

counter = letter_counter('Amazing')
counter('a') # 2
counter('m') # 1