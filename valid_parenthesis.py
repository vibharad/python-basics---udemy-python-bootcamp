
def valid_parentheses(mystr):
	openp = 0
	closep = 0
	for char in mystr:
		if char == "(":
			openp +=1
		if char == ")":
			closep +=1
		if closep > openp:
			return False
	if openp == closep:
		return True
	else:
		return False

print(valid_parentheses("()")) # True 
print(valid_parentheses(")(()))")) # False 
print(valid_parentheses("(")) # False 
print(valid_parentheses("(())((()())())")) # True 
print(valid_parentheses('))((')) # False
print(valid_parentheses('())(')) # False
print(valid_parentheses('()()()()())()(')) # False
