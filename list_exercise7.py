'''
list_manipulation([1,2,3], "remove", "end") # 3
list_manipulation([1,2,3], "remove", "beginning") #  1
list_manipulation([1,2,3], "add", "beginning", 20) #  [20,1,2,3]
list_manipulation([1,2,3], "add", "end", 30) #  [1,2,3,30]
'''

def list_manipulation(li,command,side,value=None):
    if command == "remove" and side == "end":
        return li.pop()
    elif command == "remove" and side == "beginning":
        return li.pop(0)
    elif command == "add" and side == "end":
        li.append(value)
        return li
    elif command == "add" and side == "beginning":
        li.insert(0,value)
        return li
    else:
        print("Something went wrong")
        return None


