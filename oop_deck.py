#Card
class Card:

	def __init__(self):
		from random import choice
		suits = ["Hearts", "Spades", "Diamonds", "Clubs"]
		values = ["A", "J", "K", "Q", "10","9", "8","7","6","5","4","3","2"]
		self.suit = choice (suits)
		self.value = choice (values)

	def __repr__(self):
		#print (f"{self.value} of {self.suit}")
		return f"{self.value} of {self.suit}"


# card1 = Card()
# card1.__repr__()

# card2 = Card()
# card2.__repr__()


# Deck of Cards
class Deck:

	def __init__(self):
		self.deck_size = 0
		self.new_deck = []
		while (self.deck_size < 52):
			new_card = Card()
			x = new_card.__repr__()
			if x not in self.new_deck:
				self.new_deck.append(x)
				self.deck_size +=1 
		#print(sorted(self.new_deck))

	def __repr__(self):
		return f"Deck of {self.deck_size} cards"


	def count(self):
		print (self.deck_size)
		return self.deck_size

	def shuffle(self):
		if (self.deck_size < 52):
			raise ValueError("Only full decks can be shuffled")
			return
		from random import shuffle
		shuffle(self.new_deck)
		print(self.new_deck)
		return self.new_deck

	def _deal(self,no_of_cards=1):
		if (self.deck_size == 0):
			raise ValueError("All cards have been dealt")
		elif (self.deck_size - no_of_cards < 0):
			no_of_cards = self.deck_size
			print(f"Can deal only {self.deck_size} cards")
		from random import choice
		deal_list = []
		while (no_of_cards > 0):
			deal_card = choice (self.new_deck)
			deal_list.append(deal_card)
			self.new_deck.remove(deal_card)
			self.deck_size -=1
			no_of_cards -=1
		print (f"Dealt cards : {deal_list}")
		return deal_list

	def deal_card(self):
		return self._deal()

	def deal_hand(self,num_cards):
		return self._deal(num_cards)


# ==================================Test Cases for oop_deck.py============================================ #

my_deck = Deck()
# my_deck.count()
# my_deck._deal(50)
# my_deck.count()
# my_deck._deal(3)
#my_deck._deal(1)
# print(my_deck.new_deck)
# my_deck.shuffle()
# my_deck._deal(3)
# my_deck.shuffle()
# deal_one = my_deck.deal_card()
# deal_many = my_deck.deal_hand(4)
# print (deal_many)



