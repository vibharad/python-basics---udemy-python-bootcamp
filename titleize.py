def titleize(mystr):
	words = mystr.split()
	title = ""

	for word in words:
		title += word[0].upper() + word[1:] + " "

	return title[:-1]

print(titleize("hello , my name is x"))
