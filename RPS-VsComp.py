from random import randint

#NAME OF THE GAME
print("Rock.......")
print("Paper......")
print("Scissors...")

#SINGLE PLAYER [VS COMPUTER MODE]
player1 = input("Player1 make your move: (rock/paper/scissors) ").lower()

comp = randint(0,2)
if comp == 0:
	player2="rock"
elif comp== 1:
	player2="paper"
else:
	player2="scissors"

print(f"The Computer chose {player2} \n")

#3 GAME LOGIC
if player1:

	if (player1 == player2):
		print("GAME DRAW!")

	elif player1=="rock":
		if player2=="scissors":
			print("PLAYER 1 WINS!")
		elif player2=="paper":
			print("COMPUTER WINS!")

	elif player1=="paper":
		if player2=="rock":
			print("PLAYER 1 WINS!")
		elif player2=="scissors":
			print("COMPUTER WINS!")

	elif player1=="scissors":
		if player2=="paper":
			print("PLAYER 1 WINS!")
		elif player2=="rock":
			print("COMPUTER WINS!")

	else:
		print("Something Went Wrong")