
from functools import wraps

def only_ints(fn):
	@wraps(fn)
	def wrapper(*args,**kwargs):
		flag = True
		for elem in args:
			if type(elem) is int:
				continue
			else:
				flag = False
		if flag:
			return fn(*args,**kwargs)
		else:
			return "Please only invoke with integers."
	return wrapper

@only_ints 
def add(x, y):
    return x + y
    
add(1, 2) # 3
add("1", "2") # "Please only invoke with integers."