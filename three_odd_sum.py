def three_odd_numbers(listl):
	a = 0
	b = 1
	c = 2
	if len(listl) < 3:
		return False
	for num in listl[0:-2]:
		if (listl[a]+listl[b]+listl[c]) % 2 != 0:
			print(True)
			return True
		a +=1
		b +=1
		c +=1
	print(False)
	return False


three_odd_numbers([1,2,3,4,5]) # True
three_odd_numbers([0,-2,4,1,9,12,4,1,0]) # True
three_odd_numbers([5,2,1]) # False
three_odd_numbers([1,2,3,3,2]) # False
