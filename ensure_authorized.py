'''
@ensure_authorized
def show_secrets(*args, **kwargs):
    return "Shh! Don't tell anybody!"

show_secrets(role="admin") # "Shh! Don't tell anybody!"
show_secrets(role="nobody") # "Unauthorized"
show_secrets(a="b") # "Unauthorized"
'''

from functools import wraps

def ensure_authorized(fn):
	@wraps(fn)
	def wrapper(*args,**kwargs):
		flag = False
		for a,b in kwargs.items():
			if a=="role" and b=="admin":
				flag = True
		if flag:
			print(fn(*args,**kwargs))
			return fn(*args,**kwargs)
		else:
			print("Unauthorized")
			return "Unauthorized"
	return wrapper



@ensure_authorized
def show_secrets(*args, **kwargs):
    return "Shh! Don't tell anybody!"

show_secrets(role="admin") # "Shh! Don't tell anybody!"
show_secrets(role="nobody") # "Unauthorized"
show_secrets(a="b") # "Unauthorized"
