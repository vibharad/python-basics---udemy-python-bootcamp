from string import ascii_lowercase
def is_odd_string(mystr):
	alphabet = list(ascii_lowercase)
	positions = [x for x in range(1,27)]
	alpha_pos = dict(zip(alphabet,positions))
	total = 0
	for char in mystr:
		total += alpha_pos[char]
	if total%2 == 0:
		print(False)
		return False
	print(True)
	return True

is_odd_string('a') # True
is_odd_string('aaaa') # False
is_odd_string('amazing') # True
is_odd_string('veryfun') # True
is_odd_string('veryfunny') # False