def includes(collection, value, index=None):
	if type(collection) == dict:
		return value in collection.values()

	if index is None:
		return value in collection
	return value in collection[index:]



print(includes("aeiou","a"))
print(includes("aeiou","a",1))
print(includes([1,3],1

