def make_song(line=99,drink="soda"):
	while line >= 0:
		if line == 0:
			print ("No more {}!".format(drink))
			yield "No more {}!".format(drink)
		elif line == 1:
			print ("Only 1 bottle of {} left!".format(drink))
			yield "Only 1 bottle of {} left!".format(drink)
		else:
			print("{} bottles of {} on the wall.".format(line,drink))
			yield "{} bottles of {} on the wall.".format(line,drink)
		line = line - 1
	raise StopIteration


num_lines = 5
my_song = make_song(num_lines,"vodka")
cur_line = 0
while cur_line <= num_lines+1:
	next(my_song)
	cur_line +=1
