from csv import reader, writer
def delete_users(fold,lold):
	with open("users.csv") as file:
		csv_r = reader(file)
		index = 0
		newl = []
		for row in csv_r:
			if row[0] == fold and row[1] == lold:
				index = index + 1
			else:
				newl.append(row)

	with open("users.csv", "w") as file:
		for ele in newl:
			csv_w = writer(file)
			csv_w.writerow(ele)

	return "Users deleted: {}.".format(index)

#ret = delete_users("Colt","Steele")