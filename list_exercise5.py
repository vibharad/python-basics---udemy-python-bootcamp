test = "amazing"
vowels = ['a','e','i','o','u']
answer = [char for char in test if char not in vowels]
print(answer)