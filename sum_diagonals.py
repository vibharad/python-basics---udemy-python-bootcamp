def sum_up_diagonals(l1):
	order_dec = len(l1) - 1
	order_inc = 0
	d1 = 0
	d2 = 0

	while (order_inc < len(l1)):
		d1 += l1[order_inc][order_inc]
		d2 += l1[order_inc][order_dec]
		order_inc += 1
		order_dec -= 1
	return d1 + d2


list3 = [
  [ 4, 1, 0 ],
  [ -1, -1, 0],
  [ 0, 0, 9]
]
print(sum_up_diagonals(list3))