def vowel_count(mystr):
	vowels = {'a':0,'e':0,'i':0,'o':0,'u':0}
	for char in mystr:
		if char.lower() in vowels:
			vowels[char.lower()] += 1
	return { k:v for k,v in vowels.items() if v != 0}
	



print(vowel_count("aeio are vowels in this sentence"))
