def truncate(mystr,num):
	if num < 3:
		return "Truncation must be at least 3 characters."

	if num > len(mystr):
		return mystr

	return mystr[:num-3] + "..."


print(truncate("Super cool", 2)) # "Truncation must be at least 3 characters."
print(truncate("Super cool", 1)) # "Truncation must be at least 3 characters."
print(truncate("Super cool", 0))# "Truncation must be at least 3 characters."
print(truncate("Hello World", 6))
print(truncate("Yo", 100))
print(truncate("Problem solving is the best!", 10))
print(truncate("Another test", 12))
print(truncate("Woah", 3))