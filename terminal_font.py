import pyfiglet
import termcolor

def print_font(message, colour):
	print(termcolor.colored(pyfiglet.figlet_format(message),colour))

message = input("Print String: ")
colour = input("In which colour: ")
print_font(message,colour)