def freq_count(num):
	d = {}

	for char in num:
		d[char] = 0

	for c in num:
		d[c] = d[c] + 1

	print(d)
	return d

def same_frequency(num1,num2):
	a = freq_count(str(num1))
	b = freq_count(str(num2))
	if a == b:
		return True
	return False

print(same_frequency(551122,221515))
print(same_frequency(321142,3212215))
print(same_frequency(1212, 2211))