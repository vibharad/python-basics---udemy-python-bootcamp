def combine_words(base,**kwargs):
    if len(kwargs) == 0: return base
    for position,attach in kwargs.items():
        if position == "suffix":
            return base + attach
        elif position == "prefix":
            return attach + base