def contains_keyword(*args):
    from keyword import iskeyword as contains
    for arg in args:
        if contains(arg):return True
    return False