'''
find_user("Colt", "Steele") # 1
find_user("Alan", "Turing") # 3
find_user("Not", "Here") # 'Not Here not found.'
'''
from csv import DictReader, reader, writer
def find_user(fname, lname):
    with open("users.csv") as csvfile:
        csv_r = DictReader(csvfile)
        index = 1
        for row in csv_r:
            if fname and lname in row.values():
            	return index
            else:
            	index = index + 1
        return "Not Here not found."

def print_users():
    with open("users.csv") as file:
        csv_r = reader(file)
        next(csv_r)
        for row in csv_r:
            print("{} {}".format(row[0],row[1]))

def add_user(first, last):
    with open('users.csv',"a") as file:
        csv_w = writer(file)
        csv_w.writerow([first,last])

#ret = find_user("Grace", "h")
#print (ret)