
def copy(file1, file2):
    with open(file2,"w") as dup:
        with open(file1) as ori:
            content = ori.read()
            dup.write(content)



def copy_and_reverse(file1,file2):
    with open(file2,"w") as dupr:
        with open(file1) as orig:
            content = orig.read()
            dupr.write(content[::-1])


def statistics(file):
    with open(file) as f:
        content = f.read()
    chars = len(content)
    words = len(content.split())
    lines = len(content.split("\n"))
    d = { 'lines':lines, 'words': words, 'characters' : chars }
    return d


def find_and_replace(file, orig, repl):
    with open(file, "r+") as file:
        content = file.read()
        file.seek(0)
        file.write(content.replace(orig, repl))