def once(myfunc,*args):
	once.counter = 0
	def tbinvoked(*args):
		if once.counter == 1:
			return None
		else:
			once.counter = 1
			return myfunc(args[0],args[1])
	return tbinvoked

def add(a,b):
    return a+b

oneAddition = once(add)

print(oneAddition(2,2)) # 4
oneAddition(2,2) # None
oneAddition(12,200) # None
