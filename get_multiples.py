def get_multiples(num=1,mul=10):
	count = 1
	while count <= mul:
		print (count*num)
		yield count*num
		count+=1
	raise StopIteration


my_tables = get_multiples(2,3)
next(my_tables)
next(my_tables)

print ("\n")

my_new_tables = get_multiples()
next(my_new_tables)
next(my_new_tables)
next(my_new_tables)
next(my_new_tables)
next(my_new_tables)

