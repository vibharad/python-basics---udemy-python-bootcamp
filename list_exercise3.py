test1 = [1,2,3,4]
test2 = [3,4,5,6]
answer = [num for num in test1 if (num in test1 and num in test2)]
print(answer)

test3 = ["Elie","Tim","Matt"]
answer2 = [word[::-1].lower() for word in test3]
print(answer2)
