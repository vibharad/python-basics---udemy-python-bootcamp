'''
Web scraping tool to scrape quotes
from http://quotes.toscrape.com/ 
and gameplay included to guess 
who the quote was from
'''

from bs4 import BeautifulSoup
import requests
from random import randint
from csv import DictReader, writer

def scrape():
#Getting Quotes and Quoters by Scraping using Beautiful Soup
	page_count = 1
	serial = 1
	index = []
	quotes = []
	quoters = []
	details_DOB = []
	details_location = []
	headers = ["Index","Quote","Quoter","Dob","Location"]

	with open("quotes.csv","w") as file:
		csv_w = writer(file)
		csv_w.writerow(headers)

		while (1):
			print("Fetching quotes from page " + str(page_count))
			response = requests.get("http://quotes.toscrape.com/page/"+str(page_count)+"/")
			soup_curr = BeautifulSoup(response.text, "html.parser")

			for quote in soup_curr.select(".text"):
				quotes.append(quote.get_text())

			for quoter in soup_curr.select(".author"):
				quoters.append(quoter.get_text())

			for detail in soup_curr.select(".author"):
				auth = detail.find_next_sibling()["href"]
				info = requests.get("http://quotes.toscrape.com" + auth + "/")
				soup_auth = BeautifulSoup(info.text, "html.parser")

				for date in soup_auth.select(".author-born-date"):
					details_DOB.append(date.get_text())
				
				for location in soup_auth.select(".author-born-location"):
					details_location.append(location.get_text()[3:])

			soup_next = soup_curr.select(".next")
			if not soup_next:
				break
			else:
				page_count += 1

		serial_max = len(quotes)
		while (serial < serial_max):
			index.append(serial)
			serial += 1

		csv_w.writerows(zip(index, quotes, quoters, details_DOB, details_location))
		print ("{} quotes fetched, let's play!".format(serial_max))



def play():
#Gameplay for guessing the author of quote
	with open("quotes.csv","r") as file:
		csv_r = DictReader(file)
		total_rows = 0
		for row in csv_r:
			total_rows += 1
		
		while (1):
			file.seek(0)
			pick = randint(1,total_rows)
			#print(pick)

			for row in csv_r:
				if row["Index"] == str(pick):
					question = row
			#print(question)
			print("Guess the author of the quote!")
			attempts = 0

			while (attempts < 4):
				
				if attempts == 0:
					guess = input("Quote: " + question["Quote"]+ "\n")

				elif attempts == 1:
					quoter_f = question["Quoter"].split()[0][0]
					quoter_l = question["Quoter"].split()[1][0]
					guess = input("First Name and Last Name of Quoter begin from {} and {} \n".format(quoter_f,quoter_l))

				elif attempts == 2:
					guess = input("Quoter was born on {} \n".format(question["Dob"]))

				elif attempts == 3:
					guess = input("Quoter was born in {} \n".format(question["Location"]))

				if str(guess) == str(question["Quoter"]):
					print("Great Answer!\n")
					break
				else:
					attempts+=1

			if attempts == 4:
				print("Sorry, you need to read more!")
				print("Answer was: {}".format(question["Quoter"]))

			play_next = input("Press y to continue playing or any other key to stop\n")
			if play_next.capitalize() == "Y":
				continue
			else:
				print("Thanks for playing!")
				break


scrape()
play()

























