import re 

def censor(profanity):
	profanity_pattern = re.compile(r"\b([frack][a-z]+)\b",re.I)
	censored = profanity_pattern.sub("CENSORED",profanity)
	return censored

censor("Frack you")
censor("you fracking frack ")
censor("i hope you fracking die")
