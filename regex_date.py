import re

def parse_date(datestring):
	date_pattern = re.compile(r"\b(\d{2})[,./](\d{2})[,./](\d{4})\b")
	match = date_pattern.search(datestring)
	if match:
		date_dict = {}
		date_dict["d"] = match.group(1)
		date_dict["m"] = match.group(2)
		date_dict["y"] = match.group(3)

		if int(date_dict["d"]) < 32 and int(date_dict["m"]) < 13:
			return date_dict
		else:
			return None

	else:
		return None

	
print(parse_date("12,04,2003"))
print(parse_date("12.11.2003"))
print(parse_date("01/02/1898"))
print(parse_date("01/02/18918"))