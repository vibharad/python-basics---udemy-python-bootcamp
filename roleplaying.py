class Character:
    def __init__(self,name,hp,level):
    	self.name = name
    	self.hp = hp
    	self.level = level

    def __repr__(self):
    	return f"{self.name} has an HP of {self.hp} and is at {self.level} level"


class NPC(Character):
	def __init__(self,name,hp,level):
		super().__init__(name,hp,level)

	def speak(self):
		print("Did ye hear the gunshots last night?")


bulbasaur = Character("bulbasaur",10,1)
print (bulbasaur)

pirate = NPC("Jack",25,10)
print(pirate)
pirate.speak()
