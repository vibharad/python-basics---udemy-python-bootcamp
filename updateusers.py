from csv import reader, writer
def update_users(fold,lold,fnew,lnew):
	with open("users.csv") as file:
		csv_r = reader(file)
		index = 0
		newl = []
		for row in csv_r:
			if row[0] == fold and row[1] == lold:
				newl.append([fnew,lnew])
				index = index + 1
			else:
				newl.append(row)

	with open("users.csv", "w") as file:
		for ele in newl:
			csv_w = writer(file)
			csv_w.writerow(ele)

	return "Users updated: {}.".format(index)

#ret = update_users("Colt","Steele","c","s")