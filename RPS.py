
while True:

	#NAME OF THE GAME
	print("Rock.......")
	print("Paper......")
	print("Scissors...")

	#2 PLAYER MODE
	player1 = input("Player1 make your move: (rock/paper/scissors) ").lower()
	print("========NO CHEATING========\n\n" * 40)
	player2 = input("Player2 make your move: (rock/paper/scissors) ").lower()


	#3 GAME LOGIC
	if player1 and player2:
		
		if (player1 == player2):
			print("GAME DRAW!")

		elif player1=="rock":
			if player2=="scissors":
				print("PLAYER 1 WINS!")
			elif player2=="paper":
				print("PLAYER 2 WINS!")

		elif player1=="paper":
			if player2=="rock":
				print("PLAYER 1 WINS!")
			elif player2=="scissors":
				print("PLAYER 2 WINS!")

		elif player1=="scissors":
			if player2=="paper":
				print("PLAYER 1 WINS!")
			elif player2=="rock":
				print("PLAYER 2 WINS!")
		else:
			print("Something Went Wrong")
			break

		user_decision = input("\nDo you want to keep playing? (y/n) ")
		if user_decision == "n":
			print("Thanks for playing!")
			break
		elif user_decision == "y":
			print("Okay! Let's keep going")
		else:
			print("Something Went Wrong")
			break
