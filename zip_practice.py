def interleave(a,b): #hi, no
	#step1 = [char for char in zip(a,b)] #[(h,n),(i,o)]
	step2 = "".join(["".join(tup) for tup in zip(a,b)]) #[hn,io]
	return step2 #hnio


a = input("First String: ")
b = input("Second String: ")
combined_string = interleave(a,b)
print(combined_string)