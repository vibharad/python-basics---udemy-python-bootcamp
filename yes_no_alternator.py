def yes_or_no():
	yes = True
	while True:
		if yes:
			print("yes")
			yield "yes"
			yes = False
		else:
			print("no")
			yield "no"
			yes = True

alt = yes_or_no()
next(alt)
next(alt)
next(alt)
next(alt)