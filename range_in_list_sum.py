def range_in_list(listl, start=None, end=None):
	total = 0
	if start == None and end == None:
		for val in listl:
			total += val
		return total
	if start != None and end != None:
		for val in listl[start:end+1]:
			total += val
		return total
	if start != None and end == None:
		for val in listl[start:]:
			total += val
		return total

print(range_in_list([1,2,3,4],0,2)) #  6
print(range_in_list([1,2,3,4],0,3)) # 10
print(range_in_list([1,2,3,4],1)) #  9
print(range_in_list([1,2,3,4])) # 10
print(range_in_list([1,2,3,4],0,100)) # 10
print(range_in_list([],0,1)) # 0