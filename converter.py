choice = input("For KM->Miles enter 1, For Miles->KM enter 2: ")

if choice == "1" :
	kms = input("\nHow many Kilometers? : ")
	miles = float(kms) * 0.621371
	miles = round(miles,2)
	print(f"{kms} Kilometers is {miles} Miles")

elif choice == "2" :
	miles = input("\nHow many Miles? : ")
	kms = float(miles) * 1.60934
	kms = round (kms,2)
	print(f"{miles} Miles is {kms} Kilometers")

else:
	print("WRONG CHOICE!")
