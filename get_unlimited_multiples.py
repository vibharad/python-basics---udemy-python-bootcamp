def get_unlimited_multiples(num=1):
	count = 1
	while True:
		print (count*num)
		yield count*num
		count+=1
	raise StopIteration

my_unlimited_multiples = get_unlimited_multiples(7)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)

next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)

next(my_unlimited_multiples)

next(my_unlimited_multiples)
next(my_unlimited_multiples)

next(my_unlimited_multiples)
next(my_unlimited_multiples)
next(my_unlimited_multiples)
