def running_average():
	running_average.count = 0
	running_average.times = 0
	def avg(calls):
		running_average.count += 1
		running_average.times += calls
		print (round((running_average.times/running_average.count),2))
		return round((running_average.times/running_average.count),2)
	return avg

rAvg = running_average()
rAvg(10) # 10.0
rAvg(11) # 10.5
rAvg(12) # 11