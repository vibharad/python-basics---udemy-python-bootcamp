from random import randint

number = randint(1,10)

while True:
	prompt = input("Guess a Number from 1 to 10: ")
	if number == int(prompt):
		print("You guessed it!")
		user_decision = input("Do you want to continue playing? (y/n) ")

		if user_decision == "n":
			print("Thanks for Playing!")
			break
		elif user_decision == "y":
			print("Okay let's keep going!")
			number = randint(1,10)


	elif number > int(prompt):
		print(" A little higher! ")

	else:
		print(" A little lower! ")
