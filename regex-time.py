import re

def is_valid_time(timestamp):
	time_pattern = re.compile(r'^[0-9]{1,2}:[0-9]{1,2}$')
	match = time_pattern.findall(timestamp)

	if match:
		# for time in match:
		# 	hour = int(time.split(":")[0])
		# 	minute = int(time.split(":")[1])
		return True
	else:
		return False

print(is_valid_time("10:45"))
print(is_valid_time("1:23"))
print(is_valid_time("10.45"))
print(is_valid_time("1999"))
print(is_valid_time("145:23"))