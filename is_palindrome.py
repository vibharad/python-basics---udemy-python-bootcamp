'''
is_palindrome('testing') # False
is_palindrome('tacocat') # True
is_palindrome('hannah') # True
is_palindrome('robert') # False
is_palindrome('amanaplanacanalpanama') # True
'''

def is_palindrome(word):
	if word.replace(" ","").lower()[::-1] == word.replace(" ","").lower():
		return True
	return False


w = input("Check for Palindrome: ")
check = is_palindrome(w)
if check:
	print(f"{w} is a Palindrome")
else:
	print(f"{w} is not a Palindrome")
