def reverse_vowels(instr):
	vowels = 'aeiou'
	vow = ""
	posl = []
	index = 0
	mystr = [x for x in instr]

	for char in mystr:
		if char.lower() in vowels:
			vow = vow + char
			posl.append(index)
		index += 1

	if posl:
		vow = vow[::-1]
		for pos in posl:
			mystr[pos] = vow[0]
			vow = vow[1:]

	instr = ""
	for z in mystr:
		instr += z
	return instr





reverse_vowels("Hello!") # "Holle!" 
reverse_vowels("Tomatoes") # "Temotaos" 
reverse_vowels("Reverse Vowels In A String") # "RivArsI Vewols en e Streng"
reverse_vowels("aeiou") # "uoiea"
reverse_vowels("why try, shy fly?") # "why try, shy fly?"


